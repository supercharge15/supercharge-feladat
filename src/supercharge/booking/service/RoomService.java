package supercharge.booking.service;

import supercharge.booking.dto.RoomDto;
import supercharge.booking.entity.Room;
import supercharge.booking.repository.RoomRepository;

public class RoomService {

    private static RoomService instance;

    private RoomRepository roomRepository;

    private RoomService(RoomRepository roomRepository) {
        this.roomRepository = roomRepository;
    }

    public static RoomService getInstance() {
        if (instance == null) {
            RoomRepository roomRepository = RoomRepository.getInstance();
            instance = new RoomService(roomRepository);
        }
        return instance;
    }

    public Room createRoom(RoomDto roomDto) {
        Room room = new Room();
        room.setPrice(roomDto.getPrice());
        room.setAddress(roomDto.getAddress());
        Room savedRoom = roomRepository.save(room);
        return savedRoom;
    }

}
