package supercharge.booking.service;

import supercharge.booking.dto.UserDto;
import supercharge.booking.entity.User;
import supercharge.booking.repository.UserRepository;

public class UserService {

    private static UserService instance;

    private UserRepository userRepository;

    private UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public static UserService getInstance() {
        if (instance == null) {
            UserRepository userRepository = UserRepository.getInstance();
            instance = new UserService(userRepository);
        }
        return instance;
    }

    public User createUser(UserDto userDto) {
        User user = new User();
        user.setName(userDto.getName());
        User savedUser = userRepository.save(user);
        return savedUser;
    }

}
