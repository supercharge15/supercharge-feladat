package supercharge.booking.service;

import supercharge.booking.dto.*;
import supercharge.booking.entity.Booking;
import supercharge.booking.entity.Room;
import supercharge.booking.entity.User;
import supercharge.booking.exception.BookingApplicationException;
import supercharge.booking.repository.BookingRepository;
import supercharge.booking.repository.RoomRepository;
import supercharge.booking.repository.UserRepository;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class BookingService {

    private static BookingService instance;
    private BookingRepository bookingRepository;
    private RoomRepository roomRepository;
    private UserRepository userRepository;

    private BookingService(BookingRepository bookingRepository,
                           RoomRepository roomRepository,
                           UserRepository userRepository) {
        this.bookingRepository = bookingRepository;
        this.roomRepository = roomRepository;
        this.userRepository = userRepository;
    }

    public static BookingService getInstance() {
        if (instance == null) {
            BookingRepository bookingRepository = BookingRepository.getInstance();
            RoomRepository roomRepository = RoomRepository.getInstance();
            UserRepository userRepository = UserRepository.getInstance();
            instance = new BookingService(bookingRepository, roomRepository, userRepository);
        }
        return instance;
    }

    public Booking bookRoom(BookingDto bookingDto) {
        if (bookingDto.getFrom().plusDays(1).isAfter(bookingDto.getTo())) {
            throw new BookingApplicationException("Booking must be at least a day long");
        }
        User user = userRepository.findById(bookingDto.getUserId());
        if (user == null) {
            throw new BookingApplicationException("User with id: " + bookingDto.getUserId() + "not found");
        }
        Room room = roomRepository.findById(bookingDto.getRoomId());
        if (room == null) {
            throw new BookingApplicationException("Room with id: " + bookingDto.getRoomId() + "not found");
        }
        List<Booking> bookingList = bookingRepository.findBookingsByRoomAndPeriod(bookingDto.getRoomId(),
                bookingDto.getFrom(), bookingDto.getTo());
        if (!bookingList.isEmpty()) {
            throw new BookingApplicationException("Room with id: " + bookingDto.getRoomId() + "is already reserved");
        }
        Booking booking = new Booking();
        booking.setFrom(bookingDto.getFrom());
        booking.setTo(bookingDto.getTo());
        booking.setUserId(bookingDto.getUserId());
        booking.setRoomId(bookingDto.getRoomId());
        Booking savedBooking = bookingRepository.save(booking);
        return savedBooking;
    }

    public void cancelReservation(long id, long userId) {
        Booking booking = bookingRepository.findById(id);
        if (booking == null) {
            throw new BookingApplicationException("Booking with id: " + id + "not found");
        }
        if (booking.getUserId() != userId) {
            throw new BookingApplicationException("User can not cancel others' bookings");
        }
        bookingRepository.delete(booking.getId());
    }

    public List<Room> listAvailableRooms(AvailableRoomsRequest availableRoomsRequest) {
        List<Long> reservedRoomIds = bookingRepository.findAllReservedRoomsByRoomIdsAndPeriod(availableRoomsRequest.getFrom(), availableRoomsRequest.getTo());
        List<Room> roomList = roomRepository.findAllByMaxPriceAndIdNotIn(availableRoomsRequest.getMaxPrice(), reservedRoomIds);
        return roomList;
    }

    public List<BookingHistoryDto> listBookings(BookingHistoryRequest bookingHistoryRequest) {
        List<Booking> bookingList = bookingRepository.findBookingsByUserIdAndFrom(bookingHistoryRequest.getUserId(), bookingHistoryRequest.getFromTime());
        List<BookingHistoryDto> bookingHistoryDtoList = new ArrayList<>();
        for (Booking booking : bookingList) {
            Long roomId = booking.getRoomId();
            Room room = roomRepository.findById(roomId);
            BookingHistoryDto bookingHistoryDto = new BookingHistoryDto();
            bookingHistoryDto.setAddress(room.getAddress());
            bookingHistoryDto.setPrice(room.getPrice());
            bookingHistoryDto.setFrom(booking.getFrom());
            bookingHistoryDto.setTo(booking.getTo());
            bookingHistoryDtoList.add(bookingHistoryDto);
        }
        return bookingHistoryDtoList;
    }

    public void printBookings(BookingHistoryRequest bookingHistoryRequest) {
        List<BookingHistoryDto> bookingHistoryDtoList = listBookings(bookingHistoryRequest);
        bookingHistoryDtoList.forEach(bh -> {
            System.out.println("Address: " + bh.getAddress() + ", " +
                    "Price: " + bh.getPrice() + ",  " +
                    "From: " + DateTimeFormatter.ISO_DATE_TIME.format(bh.getFrom()) +
                    ", To: " + DateTimeFormatter.ISO_DATE_TIME.format(bh.getTo()));
        });
    }

//    public List<BookingHistoryDto> listBookingsByFilters(BookingHistoryFilteredRequest bookingHistoryFilteredRequest) {
//
//    }


}
