package supercharge.booking.repository;

import supercharge.booking.entity.User;
import supercharge.booking.exception.BookingApplicationException;

import java.util.ArrayList;
import java.util.List;

import static supercharge.booking.entity.User.copyUser;

public class UserRepository {

    private static UserRepository instance;

    private List<User> userList = new ArrayList<>();
    private Long nextId;

    public static UserRepository getInstance() {
        if (instance == null) {
            instance = new UserRepository();
            instance.nextId = 1L;
        }
        return instance;
    }

    public User findById(long id) {
        return copyUser(findByIdReal(id));
    }

    private User findByIdReal(long id) {
        return userList.stream().filter(e -> e.getId() == id).findFirst().orElse(null);
    }

    public User save(User user) {
        if (user.getId() != null) {
            User dbUser = findByIdReal(user.getId());
            if (dbUser == null) {
                throw new BookingApplicationException("User not found");
            }
            dbUser.setName(user.getName());
            return copyUser(dbUser);
        } else {
            User userCopy = copyUser(user);
            generateId(userCopy);
            userList.add(userCopy);
            return copyUser(userCopy);
        }
    }

    private void generateId(User user) {
        user.setId(nextId);
        nextId++;
    }

}
