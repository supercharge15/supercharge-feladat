package supercharge.booking.repository;

import supercharge.booking.entity.Room;
import supercharge.booking.exception.BookingApplicationException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static supercharge.booking.entity.Room.copyRoom;

public class RoomRepository {

    private static RoomRepository instance;

    private List<Room> roomList = new ArrayList<>();
    private Long nextId;

    private RoomRepository() {}

    public static RoomRepository getInstance() {
        if (instance == null) {
            instance = new RoomRepository();
            instance.nextId = 1L;
        }
        return instance;
    }

    public Room findById(long id) {
        return copyRoom(findByIdReal(id));
    }

    private Room findByIdReal(long id) {
        return roomList.stream().filter(e -> e.getId() == id).findFirst().orElse(null);
    }

    public Room save(Room room) {
        if (room.getId() != null) {
            Room dbRoom = findByIdReal(room.getId());
            if (dbRoom == null) {
                throw new BookingApplicationException("Room not found");
            }
            dbRoom.setPrice(room.getPrice());
            dbRoom.setAddress(room.getAddress());
            return copyRoom(dbRoom);
        } else {
            Room roomCopy = copyRoom(room);
            generateId(roomCopy);
            roomList.add(roomCopy);
            return copyRoom(roomCopy);
        }
    }

    private void generateId(Room room) {
        room.setId(nextId);
        nextId++;
    }

    public List<Room> findAllByMaxPrice(BigDecimal maxPrice) {
        return roomList.stream().filter(e -> e.getPrice().compareTo(maxPrice) <= 0)
                .map(e -> copyRoom(e)).collect(Collectors.toList());
    }

    public List<Room> findAllByMaxPriceAndIdNotIn(BigDecimal maxPrice, List<Long> reservedRoomIds) {
        return roomList.stream().filter(e -> e.getPrice().compareTo(maxPrice) <= 0
                        && !reservedRoomIds.contains(e.getId()))
                .map(e -> copyRoom(e)).collect(Collectors.toList());
    }
}
