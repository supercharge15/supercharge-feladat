package supercharge.booking.repository;

import supercharge.booking.entity.Booking;
import supercharge.booking.exception.BookingApplicationException;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static supercharge.booking.entity.Booking.copyBooking;

public class BookingRepository {

    private static BookingRepository instance;

    private List<Booking> bookingList = new ArrayList<>();
    private Long nextId;

    private BookingRepository() {}

    public static BookingRepository getInstance() {
        if (instance == null) {
            instance = new BookingRepository();
            instance.nextId = 1L;
        }
        return instance;
    }

    public Booking findById(long id) {
        return copyBooking(findByIdReal(id));
    }

    private Booking findByIdReal(long id) {
        return bookingList.stream().filter(e -> e.getId() == id).findFirst().orElse(null);
    }

    public Booking save(Booking booking) {
        if (booking.getId() != null) {
            Booking dbBooking = findByIdReal(booking.getId());
            if (dbBooking == null) {
                throw new BookingApplicationException("Booking not found");
            }
            dbBooking.setFrom(booking.getFrom());
            dbBooking.setTo(booking.getTo());
            dbBooking.setRoomId(booking.getRoomId());
            dbBooking.setUserId(booking.getUserId());
            return copyBooking(dbBooking);
        } else {
            Booking bookingCopy = copyBooking(booking);
            generateId(bookingCopy);
            bookingList.add(bookingCopy);
            return copyBooking(bookingCopy);
        }
    }

    private void generateId(Booking booking) {
        booking.setId(nextId);
        nextId++;
    }

    public List<Long> findAllReservedRoomsByRoomIdsAndPeriod(LocalDateTime from, LocalDateTime to) {
        return bookingList.stream().filter(booking -> booking.getFrom().isBefore(to) && booking.getTo().isAfter(from))
                .map(e -> e.getRoomId())
                .collect(Collectors.toList());
    }

    public List<Booking> findBookingsByRoomAndPeriod(long roomId, LocalDateTime from, LocalDateTime to) {
        return bookingList.stream().filter(booking -> booking.getRoomId() == roomId &&
                booking.getFrom().isBefore(to) && booking.getTo().isAfter(from)).map(e -> copyBooking(e))
                .collect(Collectors.toList());
    }

    public void delete(long bookingId) {
        bookingList.removeIf(e -> e.getId() == bookingId);
    }

    public List<Booking> findBookingsByUserIdAndFrom(long userId, LocalDateTime fromTime) {
        return bookingList.stream().filter(booking -> booking.getUserId() == userId && booking.getFrom().isAfter(fromTime))
                .collect(Collectors.toList());
    }

}
