package supercharge.booking;

import supercharge.booking.dto.BookingDto;
import supercharge.booking.dto.BookingHistoryRequest;
import supercharge.booking.dto.RoomDto;
import supercharge.booking.dto.UserDto;
import supercharge.booking.service.BookingService;
import supercharge.booking.service.RoomService;
import supercharge.booking.service.UserService;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.Month;

public class Main {

    public static void main(String[] args) {
        BookingService bookingService = BookingService.getInstance();
        RoomService roomService = RoomService.getInstance();
        UserService userService = UserService.getInstance();

        RoomDto roomDto1 = new RoomDto();
        roomDto1.setAddress("Addr1");
        roomDto1.setPrice(BigDecimal.valueOf(10_000L));
        roomService.createRoom(roomDto1);

        RoomDto roomDto2 = new RoomDto();
        roomDto2.setAddress("Addr2");
        roomDto2.setPrice(BigDecimal.valueOf(20_000L));
        roomService.createRoom(roomDto2);

        UserDto userDto1 = new UserDto();
        userDto1.setName("Gipsz Jakab");
        userService.createUser(userDto1);

        UserDto userDto2 = new UserDto();
        userDto2.setName("Teszt Név");
        userService.createUser(userDto2);

        BookingDto bookingDto = new BookingDto();
        bookingDto.setRoomId(1L);
        bookingDto.setUserId(1L);
        bookingDto.setFrom(LocalDateTime.of(2022, Month.SEPTEMBER, 28, 10, 0));
        bookingDto.setTo(LocalDateTime.of(2022, Month.OCTOBER, 4, 10, 0));
        bookingService.bookRoom(bookingDto);

        BookingHistoryRequest bookingHistoryRequest = new BookingHistoryRequest();
        bookingHistoryRequest.setUserId(1L);
        bookingHistoryRequest.setFromTime(LocalDateTime.of(2022, Month.SEPTEMBER, 1, 0, 0));
        bookingService.printBookings(bookingHistoryRequest);
    }

}