package supercharge.booking.exception;

public class BookingApplicationException extends RuntimeException {

    public BookingApplicationException(String message) {
        super(message);
    }

    public BookingApplicationException(String message, Throwable cause) {
        super(message, cause);
    }
}
