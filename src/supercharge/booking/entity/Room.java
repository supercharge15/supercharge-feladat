package supercharge.booking.entity;

import java.math.BigDecimal;

public class Room {

    private Long id;
    private BigDecimal price;
    private String address;

    public static Room copyRoom(Room room) {
        Room roomCopy = new Room();
        roomCopy.setId(room.getId());
        roomCopy.setPrice(room.getPrice());
        roomCopy.setAddress(room.getAddress());
        return roomCopy;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

}
