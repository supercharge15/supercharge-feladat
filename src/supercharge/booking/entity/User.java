package supercharge.booking.entity;

public class User {

    private Long id;
    private String name;

    public static User copyUser(User user) {
        User userCopy = new User();
        userCopy.setId(user.getId());
        userCopy.setName(user.getName());
        return userCopy;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
