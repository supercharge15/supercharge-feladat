package supercharge.booking.entity;

import java.time.LocalDateTime;

public class Booking {

    private Long id;
    private Long roomId;
    private Long userId;
    private LocalDateTime from;
    private LocalDateTime to;

    public static Booking copyBooking(Booking booking) {
        Booking bookingCopy = new Booking();
        bookingCopy.setId(booking.getId());
        bookingCopy.setUserId(booking.getUserId());
        bookingCopy.setRoomId(booking.getRoomId());
        bookingCopy.setFrom(booking.getFrom());
        bookingCopy.setTo(booking.getTo());
        return bookingCopy;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRoomId() {
        return roomId;
    }

    public void setRoomId(Long roomId) {
        this.roomId = roomId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public LocalDateTime getFrom() {
        return from;
    }

    public void setFrom(LocalDateTime from) {
        this.from = from;
    }

    public LocalDateTime getTo() {
        return to;
    }

    public void setTo(LocalDateTime to) {
        this.to = to;
    }

}
