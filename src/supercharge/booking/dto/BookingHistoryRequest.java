package supercharge.booking.dto;

import java.time.LocalDateTime;

public class BookingHistoryRequest {

    private long userId;
    private LocalDateTime fromTime;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public LocalDateTime getFromTime() {
        return fromTime;
    }

    public void setFromTime(LocalDateTime fromTime) {
        this.fromTime = fromTime;
    }

}
