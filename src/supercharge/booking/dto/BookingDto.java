package supercharge.booking.dto;

import java.time.LocalDateTime;

public class BookingDto {

    private long roomId;

    private long userId;

    private LocalDateTime from;

    private LocalDateTime to;

    public BookingDto() {
    }

    public BookingDto(Long roomId, Long userId, LocalDateTime from, LocalDateTime to) {
        this.roomId = roomId;
        this.userId = userId;
        this.from = from;
        this.to = to;
    }

    public long getRoomId() {
        return roomId;
    }

    public void setRoomId(long roomId) {
        this.roomId = roomId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public LocalDateTime getFrom() {
        return from;
    }

    public void setFrom(LocalDateTime from) {
        this.from = from;
    }

    public LocalDateTime getTo() {
        return to;
    }

    public void setTo(LocalDateTime to) {
        this.to = to;
    }

}
