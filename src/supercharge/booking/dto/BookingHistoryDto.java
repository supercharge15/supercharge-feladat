package supercharge.booking.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class BookingHistoryDto {

    private LocalDateTime from;
    private LocalDateTime to;
    private String address;
    private BigDecimal price;

    public LocalDateTime getFrom() {
        return from;
    }

    public void setFrom(LocalDateTime from) {
        this.from = from;
    }

    public LocalDateTime getTo() {
        return to;
    }

    public void setTo(LocalDateTime to) {
        this.to = to;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
