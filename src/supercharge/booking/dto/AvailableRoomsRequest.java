package supercharge.booking.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class AvailableRoomsRequest {

    private BigDecimal maxPrice;
    private LocalDateTime from;
    private LocalDateTime to;

    public BigDecimal getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(BigDecimal maxPrice) {
        this.maxPrice = maxPrice;
    }

    public LocalDateTime getFrom() {
        return from;
    }

    public void setFrom(LocalDateTime from) {
        this.from = from;
    }

    public LocalDateTime getTo() {
        return to;
    }

    public void setTo(LocalDateTime to) {
        this.to = to;
    }

}
